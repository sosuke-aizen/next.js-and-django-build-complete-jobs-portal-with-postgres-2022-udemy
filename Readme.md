# Next.js & Django - Build Complete Jobs Portal with Postgres 2022 - Udemy

Next.js & Django - Build Complete Jobs Portal with Postgres 2022 - Udemy

https://www.udemy.com/course/nextjs-django-build-complete-jobs-portal-with-postgres/

https://gitlab.com/sosuke-aizen/next.js-and-django-build-complete-jobs-portal-with-postgres-2022-udemy

Build Full Stack Jobs Portal App with Next.js & Django with Postgres - with Authentication, Maps Rendering, Deployment

01. Introduction
02. Setting up Environment
03. Lets start backend with Django & Postgres
04. Starting with Jobs Resource
05. Backend - Search, Filters & Pagination
06. Backend Authentication
07. Exception Handling
08. Upload Resume to AWS S3
09. Apply To Job - Backend
10. Lets start frontend with Next.js
11. Data Fetching - Jobs
12. Search, Filters & Pagination
13. Frontend Authentication
14. Protect Routes & Handle User
15. Apply To Job - Frontend
16. Add Employer Resource
17. Deployment
18. Congratulations

What you'll learn
- How to build REST API in Django Rest Framework
- How to generate coordinates of address
- All about authentication with Simple JWT
- How to work with databases like Postgres
- How to handle exceptions
- How to add filters & pagination to API
- How to Upload Files on AWS S3
- Learn Server Side Rendering with Next JS
- How to add maps and locations in Nextjs
- Learn all different ways to pre fetch data
- How to deploy full stack app on Vercel & Heroku
- and much more...